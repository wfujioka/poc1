package com.patientdemo2.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ComponentScan
public class DataSourceConfig {

  private @Autowired UnitTestDatasourceProperties unitTestDsProps;

  @Value("${spring.datasource.jndi-name}")
  private String jndi;

  @Bean
  @Profile("unittest")
  public DataSource getUnitTestDataSource() {
    // @formatter:off
    return DataSourceBuilder.create()
        .driverClassName(unitTestDsProps.getDriverClassName())
        .url(unitTestDsProps.getUrl())
        .password(unitTestDsProps.getPassword())
        .username(unitTestDsProps.getUsername())
        .build();
    // @formatter:on
  }
}
