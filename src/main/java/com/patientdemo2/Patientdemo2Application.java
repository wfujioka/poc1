package com.patientdemo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Patientdemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(Patientdemo2Application.class, args);
	}

}
