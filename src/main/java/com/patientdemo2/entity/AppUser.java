package com.patientdemo2.entity;

import java.util.List;

public class AppUser {

  private Integer id;
  private String lastName;
  private String loginName;
  private List<AppUserRole> userRolesList;

  public List<AppUserRole> getUserRolesList() {
    return userRolesList;
  }

  public void setUserRolesList(List<AppUserRole> userRolesList) {
    this.userRolesList = userRolesList;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getLoginName() {
    return loginName;
  }

  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
