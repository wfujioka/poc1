package com.patientdemo2.config;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.j2ee.J2eePreAuthenticatedProcessingFilter;
import com.patientdemo2.security.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(jsr250Enabled = true)
public class PreSecurityConfig extends WebSecurityConfigurerAdapter {

  private @Autowired CustomUserDetailsService userDetailService;

  @Bean
  public PreAuthenticatedAuthenticationProvider preAuthenticatedAuthProvider() {
    PreAuthenticatedAuthenticationProvider provider = new PreAuthenticatedAuthenticationProvider();
    provider.setPreAuthenticatedUserDetailsService(userDetailsServiceWrapper());
    return provider;
  }

  @Override
  @Bean
  protected AuthenticationManager authenticationManager() throws Exception {
    final List<AuthenticationProvider> providers = new ArrayList<>();
    providers.add(preAuthenticatedAuthProvider());
    return new ProviderManager(providers);
  }

  @Bean
  J2eePreAuthenticatedProcessingFilter j2eePreAuthenticatedProcessingFilter() throws Exception {
    J2eePreAuthenticatedProcessingFilter filter = new J2eePreAuthenticatedProcessingFilter();
    filter.setAuthenticationManager(authenticationManager());
    filter.setContinueFilterChainOnUnsuccessfulAuthentication(false);
    filter.setCheckForPrincipalChanges(true);
    return filter;
  }

  @Override
  protected void configure(final HttpSecurity http) throws Exception {
    // @formatter:off
    http
      .addFilterAfter(j2eePreAuthenticatedProcessingFilter(), J2eePreAuthenticatedProcessingFilter.class)
      .authorizeRequests()
      .antMatchers("/**")
      .permitAll();
    // @formatter:on
  }

  UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> userDetailsServiceWrapper() {
    UserDetailsByNameServiceWrapper<PreAuthenticatedAuthenticationToken> wrapper =
        new UserDetailsByNameServiceWrapper<>();
    wrapper.setUserDetailsService(userDetailService);
    return wrapper;
  }

}
