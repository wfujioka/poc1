package com.patientdemo2.security;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import com.patientdemo2.entity.AppUser;
import com.patientdemo2.entity.AppUserRole;
import com.patientdemo2.manager.UserManager;

@Service
public class CustomUserDetailsService implements UserDetailsService {

  private @Autowired UserManager userManager;
  private static final String ROLE_PREFIX = "ROLE_";

  @Override
  public UserDetails loadUserByUsername(String username) {
    AppUser appUser = userManager.getUser(username);
    // List<GrantedAuthority> authList = new ArrayList<>();
    // authList.add(new SimpleGrantedAuthority("ROLE_USER"));
    return new User(username, "test", true, true, true, true,
        getSpringRoles(appUser.getUserRolesList()));
  }

  private List<GrantedAuthority> getSpringRoles(List<AppUserRole> userRolesList) {
    List<GrantedAuthority> authList = new ArrayList<>();
    for (AppUserRole userRole : userRolesList) {
      SimpleGrantedAuthority springRole = new SimpleGrantedAuthority(generateRoleName(userRole));
      authList.add(springRole);
    }
    return authList;
  }

  private String generateRoleName(AppUserRole userRole) {
    return ROLE_PREFIX + userRole.getRoleName();
  }

}
