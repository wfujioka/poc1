package com.patientdemo2.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.patientdemo2.entity.AppUser;

@Repository
public class UserRepo {

  private @Autowired JdbcTemplate jdbcTemplate;

  public List<AppUser> getAllUsers() {
    List<AppUser> usersList = new ArrayList<>();
    List<Map<String, Object>> rows =
        jdbcTemplate.queryForList("select app_user_login_name, last_name from msa.app_users");
    for (Map<String, Object> row : rows) {
      AppUser user = new AppUser();
      user.setLastName(row.get("last_name").toString());
      user.setLoginName(row.get("app_user_login_name").toString());
      usersList.add(user);
    }
    return usersList;
  }

  public AppUser getUsersByUsername(String username) {
    String query = "select last_name " + "from msa.app_users " + "where app_user_login_name = '"
        + username + "'";
    Map<String, Object> row = jdbcTemplate.queryForMap(query);
    AppUser user = new AppUser();
    user.setLastName(row.get("last_name").toString());
    user.setLoginName(row.get("app_user_login_name").toString());
    return user;
  }
}
