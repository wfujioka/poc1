package com.patientdemo2.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.patientdemo2.entity.AppUserRole;

@Repository
public class UserRolesRepo {

  private @Autowired JdbcTemplate jdbcTemplate;

  public List<AppUserRole> getRolesForUser(String username) {
    // @formatter:off
    String query = "select app_user_role_name "
        + "from msa.app_users u, msa.app_user_roles r, msa.xref_app_user_role_types rt "
        + "where u.app_user_login_name like '" + username + "' " 
        + "and u.app_user_id = r.app_user_id "
        + "and r.app_user_role_type_id = rt.app_user_role_type_id";
    // @formatter:on
    List<AppUserRole> userRolesList = new ArrayList<>();
    List<Map<String, Object>> rows = jdbcTemplate.queryForList(query);
    for (Map<String, Object> row : rows) {
      AppUserRole userRole = new AppUserRole();
      userRole.setRoleName(row.get("app_user_role_name").toString());
      userRolesList.add(userRole);
    }
    return userRolesList;
  }
}
